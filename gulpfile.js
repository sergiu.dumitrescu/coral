"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const gap = require("gulp-append-prepend");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const pxtorem = require("postcss-pxtorem");
const cache = require("gulp-cached");

var paths = {
  theme: {
    src: ["styles/sass/**/*.scss", "!styles/*/{bootstrap,bootstrap/**}"],
    dest: "styles/css",
  },
};

var processors = [
  autoprefixer({
    cascade: false,
  }),
  pxtorem({
    rootValue: 16,
    unitPrecision: 5,
    propList: ["*", "!letter-spacing"],
    selectorBlackList: [],
    replace: true,
    mediaQuery: false,
    minPixelValue: 0,
  }),
];

function compileTheme() {
  return gulp
    .src(paths.theme.src)
    .pipe(cache("prepend"))
    .pipe(gap.prependFile("styles/sass/_import.scss"))
    .pipe(cache("sass"))
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(cache("processors"))
    .pipe(postcss(processors))
    .pipe(gulp.dest(paths.theme.dest));
}

// function compileComponents() {
//   return gulp
//     .src(paths.components.src)
//     .pipe(gap.prependFile("styles/sass/_import.scss"))
//     .pipe(sass())
//     .on("error", sass.logError)
//     .pipe(postcss(processors))
//     .pipe(gulp.dest(paths.components.dest));
// }

function watch() {
  compileTheme();
  //   compileCheckout();
  // compileComponents();
  gulp.watch(paths.theme.src, compileTheme);
}

exports.watch = watch;
